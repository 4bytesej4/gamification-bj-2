<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gamebj' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>B!*KEdZXoGkZ`?Ch=t9C.F+>9K[p]&;Q#p;Q0_4WaX;4T)oMQQDc[a1XbXv4@:k' );
define( 'SECURE_AUTH_KEY',  '.+z^n7)G;XS0%jDe7fF}GPCzrS/Sq+_/#w-ghi1@WVV.(M=nmN{sX%p4jJJi(<fQ' );
define( 'LOGGED_IN_KEY',    '4hef$n[m2*#h&_0qW]-4ga&-Na>2{UEvWSvsNFO0A%Qnsw/- pNgN/D.rz_Axbo<' );
define( 'NONCE_KEY',        '2W6`b^Daqa`vATW.csNrikI|jp9-OPYZ9n6%p0[0wjnUhmA}1<l2GK{UWmAFc1)o' );
define( 'AUTH_SALT',        '|%1[eNM{MN$}#EEg`M!*Ektmp*IxlqEN.W<b9w~9D15[g7Rm[};p~aw@9 ?<>_?u' );
define( 'SECURE_AUTH_SALT', '!cE;${Q5JKhP]QUoQ2h$@zoT*RYXgfDUFI:7pi#ng)0O6R^W>&_mUru#@+zYA]PZ' );
define( 'LOGGED_IN_SALT',   'kZ?5ZU6(=MpC;*%3F2&Kn~+dOqj)q4WhG}!,RIM@BgQ!8ZCbaFNZK; *1<VGz&9h' );
define( 'NONCE_SALT',       'Dd>4?%7[L8lw~y7fzV5lmuX#!^;?Y4k.`y>^E&hzoA0j0(sIUrg_O(`,}QngKe)[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('VP_ENVIRONMENT', 'default');
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
